package listCollection;

import model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainImmutable {
    public static void main(String[] args) {
        Person person = new Person("Name",
                Arrays.asList("Hobby 1", "Hobby 2", "Hobby 3"));
        System.out.println(person);
        // cara salah
        // person.getHobbies().add("Hobby 4);

        // cara benar
        List<String> hobbies = new ArrayList<>(person.getHobbies());
        hobbies.add("Hobby 4");
        person.setHobbies(hobbies);
        System.out.println(person);
    }
}
