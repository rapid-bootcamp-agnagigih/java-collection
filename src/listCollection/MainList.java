package listCollection;

import model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MainList {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>(Arrays.asList("name 1", "name 2", "name 3", "name 4"));
        System.out.println(names);

        names.set(2, "nama tiga");
        System.out.println(names);

        List<Person> persons = new ArrayList<>(
                Arrays.asList(
                        new Person(1, "person 1", "city 1"),
                        new Person(2, "person 2", "city 2"),
                        new Person(3, "person 3", "city 3")
                )
        );
        for (int i = 0; i < persons.size(); i++) {
            System.out.println("Index ke " + i + " : " + persons.get(i));
        }

        System.out.println("\nSetelah Diubah");
        persons.set(1, new Person(2, "orang 2", "kota 2"));
        for (int i = 0; i < persons.size(); i++) {
            System.out.println("Index ke " + i + " : " + persons.get(i));
        }

        List<Person> personList = persons.stream().filter(x -> x.getCity().equals("city 3")).collect(Collectors.toList());
        persons.removeAll(personList);
        System.out.println("\nSetelah di-remove");
        for (int i = 0; i < persons.size(); i++) {
            System.out.println("index ke-"+ i + " : " + persons.get(i));
        }
    }
}
