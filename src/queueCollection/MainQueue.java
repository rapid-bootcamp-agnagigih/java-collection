package queueCollection;

import java.util.ArrayDeque;
import java.util.PriorityQueue;
import java.util.Queue;

public class MainQueue {
    public static void main(String[] args) {
        Queue<String> queue = new ArrayDeque<>(10);
        queue.offer("queue 1");
        queue.offer("queue 2");
        queue.offer("queue 3");
        for (String next = queue.poll(); next != null; next = queue.poll()) {
            System.out.println(next);
        }
        System.out.println(queue.size());

        // priority queue
        Queue<String> priorityQueue = new PriorityQueue<>();
        queue.offer("queue 1");
        queue.offer("queue 2");
        queue.offer("queue 3");
        for (String next = priorityQueue.poll(); next != null; next = queue.poll()) {
            System.out.println(next);
        }
        System.out.println(priorityQueue.size());
    }
}
