package sortedSet;

import model.Person;

import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

public class MainSortedSet {
    public static void main(String[] args) {
        SortedSet<Person> persons = new TreeSet<>(new PersonComparator());
        persons.add(new Person(1, "nama 1", "kota 1"));
        persons.add(new Person(2, "nama 2", "kota 2"));
        persons.add(new Person(3, "nama 3", "kota 3"));
        persons.add(new Person(4, "nama 4", "kota 4"));

        for (Person p: persons){
            System.out.println(p);
        }

        System.out.println("\nNvaigable Set");
        NavigableSet<Person> personNav = new TreeSet<>(new PersonComparator());
        personNav.add(new Person(1, "nama 1", "kota 1"));
        personNav.add(new Person(2, "nama 2", "kota 2"));
        personNav.add(new Person(3, "nama 3", "kota 3"));
        personNav.add(new Person(4, "nama 4", "kota 4"));
        for (Person person: personNav) System.out.println(person);

        System.out.println("\nNavigable dibalik");
        NavigableSet<Person> personNavDesc = personNav.descendingSet();
        for (Person person: personNavDesc) System.out.println(person);
        System.out.println();
        for (Person person: personNav.descendingSet()) System.out.println(person);
    }
}
