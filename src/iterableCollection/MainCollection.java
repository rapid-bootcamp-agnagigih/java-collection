package iterableCollection;

import model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MainCollection {
    public static void main(String[] args) {
        Collection<String> names = new ArrayList<>();
        // menambahkan
        names.add("name 1");
        names.add("name 2");
        names.addAll(List.of("name 3", "name 4"));
        System.out.println("Sebelum dihapus");
        for (String name: names) System.out.println(name);
        // menghapus
        names.remove("name 4");
        names.removeAll(Arrays.asList("name 3"));
        System.out.println("Setelah dihapus");
        for (String name: names) System.out.println(name);
        // mengecek data di collection
        System.out.println("Cek data");
        System.out.println(names.contains("name 1"));
        System.out.println(names.containsAll(Arrays.asList("name 1", "name 2")));

        Collection<Person> persons = new ArrayList<>();
        // menambahkan
        persons.add(new Person(1, "person 1", "city 1"));
        persons.add(new Person(2, "person 2", "city 2"));
        persons.addAll(Arrays.asList(new Person(3, "person 3", "city 3")));
        persons.addAll(Arrays.asList(
                new Person(4, "person 4", "city 4"),
                new Person(5, "person 5", "city 5"),
                new Person(6, "person 6", "city 6")
        ));
        for (Person person:persons) System.out.println(person);
    }
}
