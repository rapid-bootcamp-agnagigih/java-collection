package iterableCollection;

import model.Person;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MainIterable {
    public static void main(String[] args) {
        Iterable<String>  names = List.of("name 1", "name 2", "name 3", "name 4");
//        for (var name : names) System.out.println(name);

        // iterator
        Iterator<String> nameIterator = names.iterator();
        while (nameIterator.hasNext()){
            System.out.println(nameIterator.next());
        }
        Iterable<Person> persons = Arrays.asList(
                new Person(1, "person 1", "kota 1"),
                new Person(2, "person 2", "kota 2"),
                new Person(3, "person 3", "kota 3"),
                new Person(4, "person 4", "kota 4")

        );
//        for(var person: persons) System.out.println(person);

        // iterator
        Iterator<Person> personIterator = persons.iterator();
        while (personIterator.hasNext()){
            System.out.println(personIterator.next());
        }
    }
}
