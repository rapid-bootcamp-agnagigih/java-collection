package setCollection;

import model.GenderEnum;

import java.util.EnumSet;
import java.util.Set;

public class MainImmutableSet {
    public static void main(String[] args) {
        Set<GenderEnum> gender = EnumSet.allOf(GenderEnum.class);
        System.out.println(gender);

        Set<String> immuteSet = Set.of("name 1", "name 2", "name 3");
//        immuteSet.remove("Andika"); // error
//        immuteSet.add("Roni"); // error
        System.out.println(immuteSet);
    }
}
