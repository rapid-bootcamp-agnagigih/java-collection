package setCollection;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MainSet {
    public static void main(String[] args) {
        Set<String> names = new HashSet<>();
        names.add("name 1");
        names.add("name 2");
        names.addAll(List.of("name 3", "name 4", "name 5"));

        System.out.println(names);

        Set<String> hobbies = new LinkedHashSet<>();
        hobbies.add("hobby 1");
        hobbies.add("hobby 2");
        hobbies.addAll(List.of("hobby 3", "hobby 4"));

        System.out.println(hobbies);
    }
}
